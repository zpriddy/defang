import os
import sys
from defang import defang, refang


rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path = [rootdir] + sys.path

DEFANGED = "defanged"
DEFANGED_COLON = "colon"
DEFANGED_DOTS = "dots"
DEFANGED_DOTS_COLON = "dots_colon"


TEST_CASES = {
    "example.org": {
        DEFANGED: "example[.]org",
        DEFANGED_DOTS: "example[.]org",
        DEFANGED_COLON: "example[.]org",
        DEFANGED_DOTS_COLON: "example[.]org",
    },
    "http://example.org": {
        DEFANGED: "hXXp://example[.]org",
        DEFANGED_DOTS: "hXXp://example[.]org",
        DEFANGED_COLON: "hXXp[:]//example[.]org",
        DEFANGED_DOTS_COLON: "hXXp[:]//example[.]org",
    },
    "example.org\nbadguy.example.org\n": {
        DEFANGED: "example[.]org\nbadguy.example[.]org\n",
        DEFANGED_DOTS: "example[.]org\nbadguy[.]example[.]org\n",
        DEFANGED_COLON: "example[.]org\nbadguy.example[.]org\n",
        DEFANGED_DOTS_COLON: "example[.]org\nbadguy[.]example[.]org\n",
    },
    "http://1.22.33.111/path": {
        DEFANGED: "hXXp://1[.]22.33.111/path",
        DEFANGED_DOTS: "hXXp://1[.]22[.]33[.]111/path",
        DEFANGED_COLON: "hXXp[:]//1[.]22.33.111/path",
        DEFANGED_DOTS_COLON: "hXXp[:]//1[.]22[.]33[.]111/path",
    },
    "HTTP://EVIL-guy.badguy.NET": {
        DEFANGED: "hXXp://EVIL-guy.badguy[.]NET",
        DEFANGED_DOTS: "hXXp://EVIL-guy[.]badguy[.]NET",
        DEFANGED_COLON: "hXXp[:]//EVIL-guy.badguy[.]NET",
        DEFANGED_DOTS_COLON: "hXXp[:]//EVIL-guy[.]badguy[.]NET",
    },
    "ssh://foobar.example.org/": {
        DEFANGED: "(ssh)://foobar.example[.]org/",
        DEFANGED_DOTS: "(ssh)://foobar[.]example[.]org/",
        DEFANGED_COLON: "(ssh)[:]//foobar.example[.]org/",
        DEFANGED_DOTS_COLON: "(ssh)[:]//foobar[.]example[.]org/",
    },
    "ftp://foo-bar.example.org": {
        DEFANGED: "fXp://foo-bar.example[.]org",
        DEFANGED_DOTS: "fXp://foo-bar[.]example[.]org",
        DEFANGED_COLON: "fXp[:]//foo-bar.example[.]org",
        DEFANGED_DOTS_COLON: "fXp[:]//foo-bar[.]example[.]org",
    },
    "http://sub.domain.org/path/to?bad=stuff": {
        DEFANGED: "hXXp://sub.domain[.]org/path/to?bad=stuff",
        DEFANGED_DOTS: "hXXp://sub[.]domain[.]org/path/to?bad=stuff",
        DEFANGED_COLON: "hXXp[:]//sub.domain[.]org/path/to?bad=stuff",
        DEFANGED_DOTS_COLON: "hXXp[:]//sub[.]domain[.]org/path/to?bad=stuff",
    },
    "ftp://user:pass@example.com/dir": {
        DEFANGED: "fXp://user:pass@example[.]com/dir",
        DEFANGED_DOTS: "fXp://user:pass@example[.]com/dir",
        DEFANGED_COLON: "fXp[:]//user:pass@example[.]com/dir",
        DEFANGED_DOTS_COLON: "fXp[:]//user:pass@example[.]com/dir",
    },
    "ftp://user:pass@127.13.1.2/dir": {
        DEFANGED: "fXp://user:pass@127[.]13.1.2/dir",
        DEFANGED_DOTS: "fXp://user:pass@127[.]13[.]1[.]2/dir",
        DEFANGED_COLON: "fXp[:]//user:pass@127[.]13.1.2/dir",
        DEFANGED_DOTS_COLON: "fXp[:]//user:pass@127[.]13[.]1[.]2/dir",
    },
    "twitter://FooBar": {
        DEFANGED: "(twitter)://FooBar",
        DEFANGED_DOTS: "(twitter)://FooBar",
        DEFANGED_COLON: "(twitter)[:]//FooBar",
        DEFANGED_DOTS_COLON: "(twitter)[:]//FooBar",
    },
    "twitter://FooBar?go=yes": {
        DEFANGED: "(twitter)://FooBar?go=yes",
        DEFANGED_DOTS: "(twitter)://FooBar?go=yes",
        DEFANGED_COLON: "(twitter)[:]//FooBar?go=yes",
        DEFANGED_DOTS_COLON: "(twitter)[:]//FooBar?go=yes",
    },
    "twitter://FooBar?next=http://evil.com?execute=yes": {
        DEFANGED: "(twitter)://FooBar?next=hXXp://evil[.]com?execute=yes",
        DEFANGED_DOTS: "(twitter)://FooBar?next=hXXp://evil[.]com?execute=yes",
        DEFANGED_COLON: "(twitter)[:]//FooBar?next=hXXp[:]//evil[.]com?execute=yes",
        DEFANGED_DOTS_COLON: "(twitter)[:]//FooBar?next=hXXp[:]//evil[.]com?execute=yes",
    },
    "10.10.10.1/myFile": {
        DEFANGED: "10[.]10.10.1/myFile",
        DEFANGED_DOTS: "10[.]10[.]10[.]1/myFile",
        DEFANGED_COLON: "10[.]10.10.1/myFile",
        DEFANGED_DOTS_COLON: "10[.]10[.]10[.]1/myFile",
    },
    "test": {
        DEFANGED: "test",
        DEFANGED_DOTS: "test",
        DEFANGED_COLON: "test",
        DEFANGED_DOTS_COLON: "test",
    },
    "test space": {
        DEFANGED: "test space",
        DEFANGED_DOTS: "test space",
        DEFANGED_COLON: "test space",
        DEFANGED_DOTS_COLON: "test space",
    },
    "server/": {
        DEFANGED: "server[/]",
        DEFANGED_DOTS: "server[/]",
        DEFANGED_COLON: "server[/]",
        DEFANGED_DOTS_COLON: "server[/]",
    },
    "http://test": {
        DEFANGED: "hXXp://test",
        DEFANGED_DOTS: "hXXp://test",
        DEFANGED_COLON: "hXXp[:]//test",
        DEFANGED_DOTS_COLON: "hXXp[:]//test",
    },
    "http://foo?next=http://evil.com": {
        DEFANGED: "hXXp://foo?next=hXXp://evil[.]com",
        DEFANGED_DOTS: "hXXp://foo?next=hXXp://evil[.]com",
        DEFANGED_COLON: "hXXp[:]//foo?next=hXXp[:]//evil[.]com",
        DEFANGED_DOTS_COLON: "hXXp[:]//foo?next=hXXp[:]//evil[.]com",
    },
    " http://foo?next=http://evil.com": {
        DEFANGED: "hXXp://foo?next=hXXp://evil[.]com",
        DEFANGED_DOTS: "hXXp://foo?next=hXXp://evil[.]com",
        DEFANGED_COLON: "hXXp[:]//foo?next=hXXp[:]//evil[.]com",
        DEFANGED_DOTS_COLON: "hXXp[:]//foo?next=hXXp[:]//evil[.]com",
    },
    "http://foo.com?next/http://evil.com": {
        DEFANGED: "hXXp://foo[.]com?next[/]hXXp://evil[.]com",
        DEFANGED_DOTS: "hXXp://foo[.]com?next[/]hXXp://evil[.]com",
        DEFANGED_COLON: "hXXp[:]//foo[.]com?next[/]hXXp[:]//evil[.]com",
        DEFANGED_DOTS_COLON: "hXXp[:]//foo[.]com?next[/]hXXp[:]//evil[.]com",
    },
}


def test_defang():
    for fanged, options in TEST_CASES.items():
        assert defang(fanged) == options[DEFANGED]


def test_defang_colon():
    for fanged, options in TEST_CASES.items():
        assert defang(fanged, colon=True) == options[DEFANGED_COLON]


def test_defang_all_dots():
    for fanged, options in TEST_CASES.items():
        assert defang(fanged, all_dots=True) == options[DEFANGED_DOTS]


def test_defang_all_dots_and_colon():
    for fanged, options in TEST_CASES.items():
        assert defang(fanged, colon=True, all_dots=True) == options[DEFANGED_DOTS_COLON]


def test_redefang_using_test_cases():
    for fanged, options in TEST_CASES.items():
        for k, v in options.items():
            assert refang(v).lower() == fanged.lower().lstrip()


def test_refang():
    for fanged, defanged in (
        ("gopher://badstuff.org/", "(gopher)://badstuff[.]org/"),
        (
            "s3://something.amazon.com/testing?zxc=zxc",
            "s3://something[DOT]amazon(dot)com/testing?zxc=zxc",
        ),
        (
            """
https://otherstuff.org
badstuff.org
goodstuff.org/and/path
foo://newstuff.org/what?foo=true
bar-baz://crazy.stuff.other.foo.co.uk
""",
            """
hXXps://otherstuff(DOT)org
badstuff[DOT]org
goodstuff[.]org/and/path
foo://newstuff(.)org/what?foo=true
bar-baz://crazy[.]stuff(DOT)other[dot]foo(.)co.uk
""",
        ),
    ):
        assert refang(defanged) == fanged
