#!/usr/bin/env python
# coding=utf-8

import re
import sys
import warnings

PY_VERSION = sys.version_info.major


# The source of the original regex, however that was mostly to verify a url is
# valid, but it also excluded things like private IPs. There is still a lot of
# inspiration taken from it
# https://gist.github.com/dperini/729294

# These are regex fragments that are re-used in multiple places below.
RE_FRAG_AUTH = r"(?P<auth>[^@:]+(?:\:[^@]*)?@)?"
RE_FRAG_HOSTNAME = r"a-z\u00a1-\uffff0-9_\-\\"
RE_FRAG_IP = r"((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))"
RE_FRAG_PATH = r"(?P<path>/(?!/)[^?\n\r]*)?"
RE_FRAG_PORT = r"(?P<port>[0-9]{{2,5}})"
RE_FRAG_PROTO = r"(?:(?P<protocol>([+a-z]{1,12}))://)?"
RE_FRAG_QUERY = r"(?P<query>\?[^\n\r]*)?"
# This regex catches most standard URLS.
# I had to split this up into 3 regexs because python regex doesnt support
# the perl function of 'branch reset groups", so instead i implemented the same
# logic in python.

# If python where to support it it could be done with this regex:
# (?:(?:(?:(?P<protocol>[-.+a-zA-Z0-9]{1,12}):\/\/)?(?P<auth>[^@\n\r\:]+(?:\:
# [^@\n\r]*)?@)?(?|(?P<hostname>(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?
# \d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:
# (?:[a-z0-9]-*)*[a-z0-9]+)(?:\.(?:[a-z0-9]-*)*[a-z0-9]+)*)((?<tld>\.+?[a-z]{2,}
# ))(?:\.)??|([a-z0-9.-]*))((\:)((?<port>[0-9]{2,5})))?))
# ((?<query>[\/\?\=\&]+[\S]+)?)

REGEX_STANDARD_URL = re.compile(
    r"(?:[\ ]*)({proto}{auth})"
    r"(?P<hostname>(([{host}\-]+(?<=[\r\n?]))"
    r"|"
    r"([{host}\-.]+[{host}]+("
    r"?(protocol)([{host}\-]+)|(([{host}\-]+(?<=[?\r\n/]))|[{host}-]+)))"
    r"))"
    r"(?P<tld>(?:\.)[a-z]{{2,}})"
    r"(?:(?::){port})?"
    r"{path}(?=(?:(?:([+a-z]{{1,12}}))://)?){query}"
    r"".format(
        auth=RE_FRAG_AUTH,
        host=RE_FRAG_HOSTNAME,
        path=RE_FRAG_PATH,
        port=RE_FRAG_PORT,
        proto=RE_FRAG_PROTO,
        query=RE_FRAG_QUERY,
    ),
    re.IGNORECASE,
)


# This regex is needed to catch things like:
# twitter://FooBar
# myServer/myFiles
# 10.10.10.1/myFiles
# http://myServer/
# http://10.10.10.1
REGEX_SPECIAL_URL = re.compile(
    r"(?:[\ ]*)({proto}{auth})"
    # Get hostname
    r"(?P<hostname>(?(protocol)"
    # If protocol is given, use standard hostname regex
    r"([{host}\-.]+[{host}]+)|(?:"
    # if no protocol - Look for an ip with either a : or /
    r"(?:({ip})(?=[:/])" r"|"
    # or if no protocol - Look for an hostname with either a : or /
    r"([{host}.]+[{host}]+(?=[:/]))" r")))"
    # Look for TLD
    r"(?:(?P<tld>(?:\.)[a-z]{{2,}}))?"
    r"(?:(?::){port})?"
    r"{path}(?=(?:(?:([+a-z]{{1,12}}))://)?){query}"
    r"".format(
        auth=RE_FRAG_AUTH,
        host=RE_FRAG_HOSTNAME,
        ip=RE_FRAG_IP,
        path=RE_FRAG_PATH,
        port=RE_FRAG_PORT,
        proto=RE_FRAG_PROTO,
        query=RE_FRAG_QUERY,
    ),
    re.IGNORECASE,
)

# r"([{host}.]+[{host}]+(?=[:/]))" r")))"

REGEX_IP = re.compile(r"(?P<hostname>{ip}".format(ip=RE_FRAG_IP), re.IGNORECASE,)

PROTOCOL_TRANSLATIONS = {
    "http": "hXXp",
    "https": "hXXps",
    "ftp": "fXp",
}

ZERO_WIDTH_CHARACTER = "​"


def _full_match(regex, line):
    """
    Wrapper around re.findall(). Python2 version of re is missing findall()

    TODO: When no longer supporting python 2, remove this.

    :param re.Pattern regex: the regex pattern object
    :param str line: the line to search
    :return: a SRE_Match object of the match
    :raises: EnvironmentError if unknown version of python is running
    """
    if PY_VERSION == 2:
        return re.match("(?:" + regex.pattern + r")\Z", line, flags=regex.flags)
    if PY_VERSION == 3:
        return regex.fullmatch(line)
    raise EnvironmentError("unknown python version.")


def _defang_protocol(proto):
    return PROTOCOL_TRANSLATIONS.get(proto.lower(), "({0})".format(proto))


def _defang_query_string(match, all_dots=False, colon=True):
    """
    Checks the query string for more urls to defang.

    :param SRE_Match match: the regex match on the query string
    :param bool all_dots: whether to defang all dots in the URIs
    :param bool colon: whether to defang the colon in the protocol
    :return: a string of the defanged input
    """
    clean = ""
    try:
        query = match.group("query")
        if not query:
            return clean
        query_match = _find_search(query)
        if query_match:
            span = query_match.span()
            span_filler = query[: span[0]]
            span_match = _find_search(span_filler)
            if span_match:
                span_filler = span_match.string[: span_match.span()[0]] + _defang_match(
                    span_match, all_dots=all_dots, colon=colon
                )
            query = span_filler + _defang_match(
                query_match, all_dots=all_dots, colon=colon
            )
        # if (
        #     PY_VERSION == 2
        # ):  # This helps mitigate the missing functionality in python 2.7
        #     query = query.replace(".", "[.]")
        clean += query
    except IndexError:
        pass
    finally:
        return clean


def _defang_match(match, all_dots=False, colon=False):
    """
    Defangs a single regex match.

    :param SRE_Match match: the regex match on the URL, domain, ip, or subdomain
    :param bool all_dots: whether to defang all dots in the URIs
    :param bool colon: whether to defang the colon in the protocol
    :return: a string of the defanged input
    """
    clean = ""
    if not match:
        raise TypeError("no match object found")
    match_groups = match.groupdict()
    protocol = match_groups.get("protocol")
    if protocol:
        clean += _defang_protocol(protocol)
        clean += "[:]//" if colon else "://"
    auth = match_groups.get("auth")
    if auth:
        clean += auth
    hostname = match_groups.get("hostname")
    if hostname:
        clean += hostname.replace(
            ".", "[.]", -1 if all_dots else 1 if _full_match(REGEX_IP, hostname) else 0
        )
    tld = match_groups.get("tld")
    if tld:
        clean += tld.replace(".", "[.]")
    port = match_groups.get("port")
    if port:
        clean += ":{0}".format(port)
    path = match_groups.get("path")
    if path:
        clean += path
    query = _defang_query_string(match=match, all_dots=all_dots, colon=colon)
    if not protocol and hostname and path == "/" and not query:
        clean = clean[:-1] + "[/]"
    clean += query

    return clean


def _find_matches(line):
    """
    Finds the best regex match out of the three that full matches the pattern.

    :param str line: the string with URIs to be defanged
    :return: the SRE_Match of the best match
    """
    r = _full_match(REGEX_STANDARD_URL, line)
    if r:
        return r
    r = _full_match(REGEX_SPECIAL_URL, line)
    if r:
        return r
    r = _full_match(REGEX_IP, line)
    if r:
        return r


def _find_search(line):
    """
    Find the best regex match using search.

    :param str line: the string with URIs to be defanged
    :return: the SRE_Match of the best match
    """
    r = REGEX_STANDARD_URL.search(line)
    if r:
        return r
    r = REGEX_SPECIAL_URL.search(line)
    if r:
        return r
    r = REGEX_IP.search(line)
    if r:
        return r


def defang(line, all_dots=False, colon=False, zero_width_replace=False):
    """
    Defangs a line of text.

    :param str line: the string with URIs to be defanged
    :param bool all_dots: whether to defang all dots in the URIs
    :param bool colon: whether to defang the colon in the protocol
    :param bool zero_width_replace: inserts a zero width character after every character
    :return: the defanged string
    """
    if zero_width_replace:
        return ZERO_WIDTH_CHARACTER.join(line)
    lines = line.splitlines()
    defanged_lines = []
    for l in lines:
        match = _find_matches(l)
        if not match:
            defanged_lines.append(l)
            continue
        defanged_lines.append(_defang_match(match, all_dots, colon))

    # Because I split this up by lines, I need to add a nre line if its missing.
    return "\n".join(defanged_lines) + ("\n" if line[-1:] == "\n" else "")


def defanger(infile, outfile):
    """
    Takes an input file-like object, and writes the defanged content to the
    outfile file-like object.

    :param file-like infile: the object to be read from and defanged
    :param file-like outfile: the file-like object to write the defanged output
    :return: None
    """
    for line in infile:
        clean_line = defang(line)
        outfile.write(clean_line)


def refang(line):
    """
    Refangs a line of text.

    :param str line: the line of text to reverse the defanging of.
    :return: the "dirty" line with actual URIs
    """
    if all(char == ZERO_WIDTH_CHARACTER for char in line[1::2]):
        return line[::2]
    dirty_line = re.sub(r"\((\.|dot)\)", ".", line, flags=re.IGNORECASE)
    dirty_line = re.sub(r"\[(\.|dot)\]", ".", dirty_line, flags=re.IGNORECASE)
    dirty_line = re.sub(
        r"(\s*)h([x]{1,2})p([s]?)\[?:\]?//",
        r"\1http\3://",
        dirty_line,
        flags=re.IGNORECASE,
    )
    dirty_line = re.sub(
        r"(\s*)(s?)fxp(s?)\[?:\]?//", r"\1\2ftp\3://", dirty_line, flags=re.IGNORECASE
    )
    dirty_line = re.sub(r"([a-z]+)(\[?/\])", r"\1/", dirty_line, flags=re.IGNORECASE,)
    dirty_line = re.sub(
        r"(\s*)\(([-.+a-zA-Z0-9]{1,12})\)\[?:\]?//",
        r"\1\2://",
        dirty_line,
        flags=re.IGNORECASE,
    )
    return dirty_line


def refanger(infile, outfile):
    """
    Takes an input file-like object, and writes the refanged content to the
    outfile file-like object.

    :param file-like infile: the object to be read from and refanged
    :param file-like outfile: the file-like object to write the refanged output
    :return: None
    """
    for line in infile:
        dirty_line = refang(line)
        outfile.write(dirty_line)
